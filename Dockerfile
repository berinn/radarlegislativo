FROM debian:testing

RUN apt-get update && apt-get install -q -y python-pip

ADD requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

ADD ./radarlegislativo /srv/radarlegislativo

# Precisamos das variáveis de ambiente no docker para rodar o
# collectstatic e o syncdb.
ADD .env /srv/radarlegislativo/.env

WORKDIR /srv/radarlegislativo

RUN python manage.py collectstatic --noinput

EXPOSE 8000
CMD ["gunicorn", "-b", "0.0.0.0:8000", "radarlegislativo.wsgi:application"]
