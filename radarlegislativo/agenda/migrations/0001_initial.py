# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-03 02:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comissao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('origem', models.CharField(choices=[('CA', 'C\xe2mara'), ('SE', 'Senado')], max_length=2)),
                ('nome', models.CharField(max_length=255)),
                ('link', models.URLField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Evento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.DateField()),
                ('titulo', models.CharField(max_length=255)),
                ('link', models.URLField(blank=True)),
                ('descricao', models.TextField()),
                ('comissao', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='agenda.Comissao')),
            ],
        ),
    ]
