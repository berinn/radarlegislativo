# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import datetime
from django.test import TestCase
from freezegun import freeze_time
from model_mommy import mommy

from agenda.models import Evento


__all__ = ["TesteAgendaManager"]


@freeze_time('2018-04-05')
class TesteAgendaManager(TestCase):

    def setUp(self):
        self.evento_novo = mommy.make('Evento')

        self.evento_antigo = mommy.make('Evento')
        self.evento_antigo.data = datetime.date(2018, 3, 31)
        self.evento_antigo.save()

    def teste_encontra_evento_do_dia_05_na_mesma_semana_que_o_dia_06(self):
        self.assertIn(
            self.evento_novo,
            Evento.objects.na_mesma_semana_que_o_dia(
                datetime.date(2018, 4, 6)),
        )

    def teste_nao_encontra_evento_do_dia_31_na_mesma_semana_que_o_dia_06(self):
        self.assertNotIn(
            self.evento_antigo,
            Evento.objects.na_mesma_semana_que_o_dia(
                datetime.date(2018, 4, 6)),
        )
