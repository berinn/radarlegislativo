# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.test import TestCase
from main.models import Projeto
from django.urls import reverse


__all__ = ["TestPaginaDeUmProjeto"]


class TestPaginaDeUmProjeto(TestCase):
    fixtures = ["projetos", "tramitacoes", "tags"]

    def setUp(self):
        self.projeto = Projeto.objects.first()

    def test_pagina_de_um_projeto_retorna_200(self):
        response = self.client.get(
            reverse("website:projeto", args=[self.projeto.id]))
        self.assertEqual(response.status_code, 200)

    def test_pagina_de_um_projeto_usa_o_template_certo(self):
        response = self.client.get(
            reverse("website:projeto", args=[self.projeto.id]))
        self.assertTemplateUsed(response, "website/projeto.html")

    def test_pagina_de_um_projeto_inclui_ele_no_contexto(self):
        response = self.client.get(
            reverse("website:projeto", args=[self.projeto.id]))
        self.assertEqual(response.context["projeto"], self.projeto)
