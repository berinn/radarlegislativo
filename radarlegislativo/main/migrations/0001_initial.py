# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-29 16:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Projeto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('origem', models.CharField(choices=[('CA', 'C\xe2mara'), ('SE', 'Senado')], max_length=2)),
                ('id_site', models.IntegerField()),
                ('nome', models.CharField(max_length=255)),
                ('apresentacao', models.DateField()),
                ('ementa', models.TextField()),
                ('situacao', models.CharField(max_length=255)),
                ('autoria', models.CharField(max_length=255)),
                ('apensadas', models.CharField(blank=True, max_length=255)),
                ('html_original', models.TextField(blank=True)),
                ('important', models.BooleanField(default=False)),
                ('urgent', models.BooleanField(default=False)),
                ('aditional_information', models.TextField(blank=True)),
                ('clippings', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='Tramitacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.DateField()),
                ('local', models.CharField(max_length=255)),
                ('descricao', models.TextField()),
                ('projeto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Projeto')),
            ],
        ),
        migrations.AddField(
            model_name='projeto',
            name='tags',
            field=models.ManyToManyField(to='main.Tag'),
        ),
    ]
