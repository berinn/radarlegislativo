# Draft Bills relevant to privacy, freedom of expression, access and gender issues in digital media

Source code of the website <https://radarlegislativo.org/>.

Maintained by [Coding Rights](https://codingrights.org), an organization led by women dedicated to promoting the comprehension about how digital technologies work and to expose power assimetries that may be amplified by its usage.

Our work envolves monitoring and analyzing legal, cultural and programming codes to influence public policy and promote good practices. We are part of a global network of activists that create and share tools and strategies for a more autonomous and conscious usage of technologies and for the inclusion of the human rights perspective when we think about digital media.

If you want to understand the source code of the website, seea [CONTRIBUTING.en.md][contributing].

[contributing]: https://gitlab.com/codingrights/pls/blob/master/CONTRIBUTING.en.md
